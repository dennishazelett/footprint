require(rtracklayer)
require(GenomicRanges)

BedTool <- function(bio.features.loc = NULL) {
  ## modified from S. Coetzee's FunciSNP2::GetBioFeatures
  if (!is.null(bio.features.loc)) {
    if (length(bio.features.loc >= 1)) {
      bed.list <- lapply(bio.features.loc,
                         function(x) {
        x.gr <- read.table(x, stringsAsFactors = FALSE) #import.bed(x, asRangedData=F)
        x.gr <- GRanges(seqnames = x.gr[,1],
                        ranges = IRanges(x.gr[,2], x.gr[,3]))
        seq.ranges <- c(paste("chr", 1:22, sep=""),
                        "chrX", "chrY")
        x.gr <- sort(x.gr)
        seqlevels(x.gr, force=TRUE) <- seq.ranges
        mcols(x.gr) <- NULL
        name <- str_split(x, pattern="\\.")[[1]]
        name <- name[1] #name[-length(name)]
        mcols(x.gr)[, "feature"] <- name
        strand(x.gr) <- "*"
        return(x.gr)
      })
    } else {
      bed.list <- NULL
    }
    if (is.null(bed.list)) {
      return(GRangesList())
    } else {
      return(GRangesList(bed.list))
    }
  } else {
    return(NULL)
  }
}

intersectBed <- function (xa, xb, wa=FALSE) {
  if (wa) {
    #newbed <- subsetByOverlaps(xa, xb)
    newbed <- xa[unique(queryHits(findOverlaps(xa, xb)))]
  } else {
    newbed <- intersect(xa, xb)
  } # x is a BedTool object
  if (length(newbed)==0) {warning("No overlapping regions reported from intersectBed")}
                                        #reduce(newbed)
  newbed
}

subtractBed  <- function (xa, xb, A=FALSE) {
  if (A) {
    xa[-unique(queryHits(findOverlaps(xa, xb)))]
  } else {
    setdiff(xa, xb)
  }
}

mergeBed <- function (x) {
  reduce(x)
}

sortBed <- function (x) {
  sort(x)
}
  
multiIntersectBed <- function (x) {
  reduce(unlist(unname(x)))
}

UCSC_columns <- function (GRobj, itemRgb) {
  GRangesForUCSCGenome("hg19", chrom = GRobj@seqnames,
                       GRobj@ranges,
                       itemRgb = itemRgb)
}

saveBed <- function (GRobj, filename, ...) {
  export.bed(con = filename,
             object = GRobj,
             ...)
} 
