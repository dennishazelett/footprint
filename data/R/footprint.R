# promoter marks take precedence over other marks in defining features, particularly promoters
promoter_marks_defined   <<- c('K4M3', 'H3K4ME3', 'H3K4ME3::NARROWPEAK')
# passive regulato`ry marks include any histone mods that are broadly enhancer OR promoter, but generally don't help to distinguish
regulatory_marks_passive <<- c('K4M1', 'H3K4ME1', 'H3K4ME1::NARROWPEAK', 'K4M2', 'DHS::BROADPEAK')
# active regulatory marks delineate the region of enhancers and promoters, but don't distinguish
regulatory_marks_active  <<- c('K27AC', 'H3K27AC', 'K27AC::NARROWPEAK', 'H3K27AC::NARROWPEAK', 'K9AC', 'H3K9AC::NARROWPEAK', 'H3K9AC')
# broad active marks similarly do not distinguish, delineating broad regions of low active status; associated with but not identified as regulatory sequence
active_marks_broad       <<- c('K27AC::BROADPEAK', 'H3K27AC::BROADPEAK', 'H3K4ME1::BROADPEAK',
                               'H3K4ME2::BROADPEAK', 'K4M2::BROADPEAK', 'H3K4ME3::BROADPEAK',
                               'H3K9AC::BROADPEAK', 'K4M2::BROADPEAK', 'H2A.Z::BROADPEAK')
# core enhancer marks; the region within an enhancer most likely defined as regulatory--i.e. TF bound and open chromatin
enhancer_marks_core      <<- c('DHS', 'DNASEI', 'DNASE', 'DNASE::NARROWPEAK', 'DNASE::HOTSPOT', 'FAIRE', 'HOT', 'TF', 'TROUGH', 'CTCF::NARROWPEAK')
# transcription factor binding: self-explanatory
transcription_factor     <<- c('FOXA1', 'NKX31', 'NKX3-1', 'AR', 'AROR', 'TCF7L1', 'TAL1', 'CEBPB',
                               'ATF1', 'NR3C1', 'TEAD1', 'ZBTB33', 'SUZ12', 'MYC', 'MAX', 'ZNF217',
                               'YY1', 'ZNF274', 'USF1')
# repressive marks
repressive_marks         <<- c('K27M3', 'H3K27ME3', 'H3K27ME3::BROADPEAK')

heterochromatin          <<- c('K9M3', 'H3K9ME3', 'H3K9ME3::BROADPEAK')

transcribed_marks        <<- c('K36M3', 'H3K36ME3', 'K36M3::NARROWPEAK', 'K36M3::BROADPEAK', 'H3K36ME3::BROADPEAK')

TSS                      <<- c('TSS')
ctcf_sites               <<- c('CTCF')

all_regulatory_marks     <<- unique(c(promoter_marks_defined,
                                      regulatory_marks_active,
                                      regulatory_marks_passive,
                                      active_marks_broad))

open_chromatin           <<- unique(c(enhancer_marks_core, transcription_factor))


footprint <- function(CELLTYPE, MANIFEST) {
  #
  if (CELLTYPE=='ALL') return(NULL)
  print(paste(CELLTYPE, "SEGMENTATION INITIATED"))
  #
  make_footprint <- function(files) {
    if (nrow(files) > 1) {
      footprint <- multiIntersectBed(BedTool(files$FILE))
    } else if (nrow(files) == 1) {
      footprint <- reduce(unlist(BedTool(files$FILE[1])))
    } else {
      footprint <- NULL
    }
    footprint
  }
  #
  MANIFEST$MARK <- toupper(MANIFEST$MARK)
  regulatory_cell <- subset(subset(MANIFEST, MARK %in% all_regulatory_marks), SAMPLE==CELLTYPE)
  HAVE_REGULATORY_MARKS <- nrow(regulatory_cell) > 0
  regulatory_footprint <- make_footprint(regulatory_cell)
  #
  # UNCOMMENT to debug regulatory footprint
  # if (HAVE_REGULATORY_MARKS) {
  #   saveBed(regulatory_footprint, filename=paste(c('tmp/', CELLTYPE, '.regulatory_footprint.bed'), collapse = ''))
  # }
  #
  # grab table entries for CELLTYPE relevant to generic enhancers; create a flat footprint from them
  enhancer_cell       <- subset(subset(MANIFEST, MARK %in% c(regulatory_marks_active, regulatory_marks_passive)), SAMPLE==CELLTYPE)
  HAVE_ENHANCER_MARKS <- nrow(enhancer_cell) > 0
  enhancer_footprint  <- make_footprint(enhancer_cell)
  #
  # grab table entries for CELLTYPE relevant to passive enhancers; create a flat footprint from them
  passive_cell        <- subset(subset(MANIFEST, MARK %in% regulatory_marks_passive), SAMPLE==CELLTYPE)
  HAVE_PASSIVE_MARKS  <- nrow(passive_cell) > 0
  passive_footprint   <- make_footprint(passive_cell)
  
  # grab table entries for CELLTYPE relevant to active regulatory regions; create a flat footprint from them
  active_cell         <- subset(subset(MANIFEST, MARK %in% regulatory_marks_active), SAMPLE==CELLTYPE)
  HAVE_ACTIVE_MARKS   <- nrow(active_cell) > 0  
  active_footprint    <- make_footprint(active_cell)

  # grab table entries for CELLTYPE relevant to active regions; create a flat footprint from them
  active_region       <- subset(subset(MANIFEST, MARK %in% active_marks_broad), SAMPLE==CELLTYPE)
  HAVE_ACTIVE_BROAD   <- nrow(active_region) > 0
  active_region_foot  <- make_footprint(active_region)
  
  # grab table entries for CELLTYPE relevant to open chromatin; create a flat footprint from them
  open_cell           <- subset(subset(MANIFEST, MARK %in% open_chromatin), SAMPLE==CELLTYPE)
  HAVE_OPEN_CHROMATIN <- nrow(open_cell) > 0  
  open_chromatin_footprint <- make_footprint(open_cell)
  
  # grab table entries for CELLTYPE relevant to generic promoters; create a flat footprint from them
  promarks_cell       <- subset(subset(MANIFEST, MARK %in% promoter_marks_defined), SAMPLE==CELLTYPE)
  HAVE_PROMARKS       <- nrow(promarks_cell)

  # grab table entries for CELLTYPE relevant to CTCF sites; create a flat footprint from them
  ctcf_cell           <- subset(subset(MANIFEST, MARK %in% ctcf_sites), SAMPLE==CELLTYPE)
  HAVE_CTCF           <- nrow(ctcf_cell)
  ctcf_footprint      <- make_footprint(ctcf_cell)

  # grab table entries for CELLTYPE relevant to repressed regions; create a flat footprint from them
  repressed           <- subset(subset(MANIFEST, MARK %in% repressive_marks), SAMPLE==CELLTYPE)
  HAVE_REPR_MARKS     <- nrow(repressed)
  repressed_footprint <- make_footprint(repressed)

  # grab table entries for CELLTYPE relevant to heterochromatin; create a flat footprint from them
  heterochrom_region  <- subset(subset(MANIFEST, MARK %in% heterochromatin), SAMPLE==CELLTYPE)
  HAVE_HETC_MARKS     <- nrow(heterochrom_region)
  heterochr_footprint <- make_footprint(heterochrom_region)

  # grab table entries for CELLTYPE relevant to transcribed regions; create a flat footprint from them
  transcribed_region  <- subset(subset(MANIFEST, MARK %in% transcribed_marks), SAMPLE==CELLTYPE)
  HAVE_TRANSCR_MARKS  <- nrow(transcribed_region)
  transcr_footprint   <- make_footprint(transcribed_region)
  # if there are no promoter marks, promoters can still be defined relative to TSS;
  # However, where there are no other epigenetic features in the TSS region, the promoter 
  # should not be segmented. The following code block implements this logic.
  if(nrow(subset(MANIFEST, MARK=='TSS')) < 1) {
    message('using builtin TSS - refseq; minus exons; minus 3\' utr')
    tss_footprint <- reduce(unlist(BedTool('/home/rstudio/promoter.no_utr3.no_exon.bed')))
  } else {
    tss_footprint <- reduce(unlist(BedTool(subset(MANIFEST, MARK=='TSS')$FILE[1])))
  }
  if (HAVE_PROMARKS) {
    promoter_footprint <- make_footprint(promarks_cell)
    #promoter_footprint <- intersectBed(promoter_footprint, tss_footprint, wa=TRUE)
    if (HAVE_ENHANCER_MARKS) {
      enhancer_footprint <- subtractBed(enhancer_footprint, promoter_footprint)
    }
  } else {
    promoter_footprint <- tss_footprint; rm(tss_footprint)
    if (HAVE_ENHANCER_MARKS) {
      promoter_footprint <- intersectBed(promoter_footprint, enhancer_footprint, wa=TRUE)
      enhancer_footprint <- subtractBed(enhancer_footprint, promoter_footprint)
    }
  }
  ##
  ## Identify and segregate active promoters and enhancers
  ##
  if (HAVE_ACTIVE_MARKS) {
    active_promoter_footprint <- intersectBed(promoter_footprint, active_footprint, wa=TRUE)
    active_enhancer_footprint <- subtractBed(reduce(intersectBed(enhancer_footprint, active_footprint)), active_promoter_footprint) #, wa=TRUE), active_promoter_footprint)
    if (HAVE_PASSIVE_MARKS) {
      poised_promoter_footprint <- subtractBed(promoter_footprint, active_footprint, A=TRUE)
      non_active_reg_footprint  <- subtractBed(enhancer_footprint, active_enhancer_footprint) #, A=TRUE)
      poised_enhancer_footprint <- subtractBed(non_active_reg_footprint, promoter_footprint)
    } else {
      poised_enhancer_footprint <- GRanges()
    }
  } else {
    active_promoter_footprint <- GRanges()
    poised_promoter_footprint <- GRanges()
  }
  ##
  ## Identify open chromatin regions
  ## (DEFINITION INCLUDES TF BINDING)
  ##
  if (HAVE_OPEN_CHROMATIN) {
    if (HAVE_ACTIVE_MARKS) {
      active_promoter_core_footprint <- intersectBed(open_chromatin_footprint, active_promoter_footprint, wa = TRUE)
      active_enhancer_core_footprint <- intersectBed(open_chromatin_footprint, active_enhancer_footprint, wa = TRUE)
      putative_regulatory_sites      <- subtractBed(open_chromatin_footprint, active_promoter_core_footprint)
      putative_regulatory_sites      <- subtractBed(putative_regulatory_sites, active_enhancer_core_footprint)
      if (HAVE_PASSIVE_MARKS) {
        poised_promoter_core_footprint <- intersectBed(open_chromatin_footprint, poised_promoter_footprint, wa = TRUE)
        poised_enhancer_core_footprint <- intersectBed(open_chromatin_footprint, poised_enhancer_footprint, wa = TRUE)
        poised_enhancer_core_footprint <- subtractBed(poised_enhancer_core_footprint, poised_promoter_core_footprint)
        putative_regulatory_sites    <- subtractBed(putative_regulatory_sites, poised_enhancer_core_footprint)
        putative_regulatory_sites    <- subtractBed(putative_regulatory_sites, poised_promoter_core_footprint)
      }
    } else if (HAVE_ENHANCER_MARKS) {
      enhancer_core_footprint    <- intersectBed(open_chromatin_footprint, enhancer_footprint, wa = TRUE)
      promoter_core_footprint    <- intersectBed(open_chromatin_footprint, promoter_footprint, wa = TRUE)
      putative_regulatory_sites  <- subtractBed(open_chromatin_footprint, enhancer_core_footprint)
      putative_regulatory_sites  <- subtractBed(putative_regulatory_sites, promoter_core_footprint)
    } else if (HAVE_PROMARKS) {
        promoter_core_footprint   <- intersectBed(open_chromatin_footprint, promoter_footprint, wa = TRUE)
        promoter_footprint        <- subtractBed(promoter_footprint, promoter_core_footprint)
        putative_regulatory_sites <- subtractBed(open_chromatin_footprint, promoter_core_footprint)
    } else {
      putative_regulatory_sites  <- open_chromatin_footprint
    }
  }

  if (HAVE_REPR_MARKS) {
    if (HAVE_OPEN_CHROMATIN) {
      repressed_region_footprint <- sortBed(subtractBed(repressed_footprint, open_chromatin_footprint))
    } else {
      repressed_region_footprint <- sortBed(repressed_footprint)
    }
  }
  ##
  if (HAVE_HETC_MARKS) {
    heterochrom_region_footprint <- sortBed(heterochr_footprint)
  }
  ##
  ## final refinements, subtract core regions from enhancers and promoters and rename
  ##
  if (HAVE_ACTIVE_MARKS) { # i.e. HAVE_ACTIVE_MARKS
    if (HAVE_OPEN_CHROMATIN) {
      PAR <- subtractBed(active_promoter_footprint, open_chromatin_footprint)
      EAR <- subtractBed(active_enhancer_footprint, open_chromatin_footprint)
      if (HAVE_CTCF) {
        PAR <- subtractBed(PAR, ctcf_footprint)
        EAR <- subtractBed(EAR, ctcf_footprint)
      }
      if (HAVE_PASSIVE_MARKS) {
        PPR  <- subtractBed(poised_promoter_footprint, open_chromatin_footprint)
        EPR  <- subtractBed(poised_enhancer_footprint, open_chromatin_footprint)
        PPRC <- poised_promoter_core_footprint
        EPRC <- subtractBed(poised_enhancer_core_footprint, active_enhancer_core_footprint)
        if (HAVE_CTCF) {
          PPR <- subtractBed(PPR, ctcf_footprint)
          EPR <- subtractBed(EPR, ctcf_footprint)
          CTCFCORE <- intersectBed(sortBed(c(PPRC, EPRC)), ctcf_footprint)
          PPRC <- subtractBed(PPRC, ctcf_footprint)
          EPRC <- subtractBed(EPRC, ctcf_footprint)
        }
      }
      EARC <- subtractBed(active_enhancer_core_footprint, promoter_footprint)
      PARC <- subtractBed(active_promoter_core_footprint, EARC)
      PARC <- sortBed(subtractBed(PARC, EPRC))
      RPS  <- putative_regulatory_sites
      if (HAVE_CTCF) {
        CTCF <- subtractBed(ctcf_footprint, sortBed(c(putative_regulatory_sites, EARC, PARC)))
        CTCFCORE <- intersectBed(sortBed(c(putative_regulatory_sites, EARC, PARC)), ctcf_footprint)
        EARC <- subtractBed(EARC, ctcf_footprint)
        PARC <- subtractBed(PARC, ctcf_footprint)
        RPS <- subtractBed(RPS, ctcf_footprint)
      }
    } else {
      PAR <- active_promoter_footprint
      EAR <- active_enhancer_footprint
      PPR <- poised_promoter_footprint
      EPR <- poised_enhancer_footprint
      if (HAVE_CTCF) {
        PAR <- subtractBed(PAR, ctcf_footprint)
        EAR <- subtractBed(EAR, ctcf_footprint)
        CTCF <- ctcf_footprint
      }
    }
  } else { # case NO ACTIVE MARKS
    if (HAVE_OPEN_CHROMATIN) {
      if (HAVE_ENHANCER_MARKS) {
        PRC <- promoter_core_footprint
        ERC <- subtractBed(enhancer_core_footprint, PRC)
        if (HAVE_CTCF) {
          CTCFCORE <- intersectBed(sortBed(c(PRC, ERC, putative_regulatory_sites)), ctcf_footprint)
          CTCF <- subtractBed(ctcf_footprint, CTCFCORE)
          PRC <- subtractBed(PRC, ctcf_footprint)
          ERC <- subtractBed(ERC, ctcf_footprint)
        }
      } else if (HAVE_PROMARKS) {
        PRC <- promoter_core_footprint
        if (HAVE_CTCF) {
          CTCFCORE <- intersectBed(sortBed(c(PRC, putative_regulatory_sites)), ctcf_footprint)
          CTCF <- subtractBed(ctcf_footprint, CTCFCORE)
          PRC <- subtractBed(PRC, ctcf_footprint)
        }
      }
      if (HAVE_CTCF) {
        CTCFCORE <- intersectBed(ctcf_footprint, putative_regulatory_sites)
        CTCF <- subtractBed(ctcf_footprint, putative_regulatory_sites)
        RPS <- subtractBed(putative_regulatory_sites, ctcf_footprint)
      } else {
        RPS <- putative_regulatory_sites
      }
    }
    if (HAVE_ENHANCER_MARKS) {
      if (HAVE_OPEN_CHROMATIN) {
        ER <- subtractBed(enhancer_footprint, ERC)
      } else {
        ER <- enhancer_footprint
      }
      if (HAVE_CTCF) {
        ER <- subtractBed(ER, ctcf_footprint)
      }
    }
    if (HAVE_PROMARKS) {
      if (HAVE_OPEN_CHROMATIN) {
        PR <- subtractBed(promoter_footprint, PRC)
      } else {
        PR <- promoter_footprint
      }
      if (HAVE_CTCF) {
        PR <- subtractBed(PR, ctcf_footprint)
        CTCF <- ctcf_footprint
      }
    }
  }
  ##
  if (HAVE_REPR_MARKS) {
    SCR <- repressed_region_footprint #silenced chromatin region
    if (HAVE_REGULATORY_MARKS) {
      SCR <- sortBed(subtractBed(SCR, regulatory_footprint))
    }
  }
  ##
  if (HAVE_HETC_MARKS) {
    HET <- heterochrom_region_footprint
    if (HAVE_REGULATORY_MARKS) {
      HET <- sortBed(subtractBed(HET, regulatory_footprint))
    }
    if (HAVE_REPR_MARKS) {
      HET <- sortBed(subtractBed(HET, repressed_footprint))
    }
  }

  if (HAVE_TRANSCR_MARKS) {
    TRS <- transcr_footprint
    if (HAVE_REGULATORY_MARKS) {
      TRS <- subtractBed(TRS, regulatory_footprint)
    }
    # remaining open chromatin is offen associated with transcribed exons,
    # therefore transcribed takes precedence 
    if (HAVE_OPEN_CHROMATIN) {
      RPS <- subtractBed(RPS, TRS)
    }
    if (HAVE_CTCF) {
      TRS <- subtractBed(TRS, ctcf_footprint)
    }
    # active transcription takes precedence over repressed or silenced states
    if (HAVE_REPR_MARKS) {
      SCR <- subtractBed(SCR, TRS)
    }
    if (HAVE_HETC_MARKS) {
      HET <- subtractBed(HET, TRS)
    }
  }
  
  ############################################################
  ## COLOR PALETTE (based on ENCODE ChromHMM segmentations) ##
  ############################################################

  lightred    <- '#FFCCCC' #"255,204,204"
  deepred     <- '#9B0000' #"155,0,0"
  lightgreen  <- '#E5FFCC' #"229,255,204"
  deepgreen   <- '#009B00' #"0,155,0"
  lightorange <- '#FFE5CC' #"255,229,204"
  deeporange  <- '#FF8000' #"255,128,0"
  lightyellow <- '#E1E133' #"225,225,51"
  lightblue   <- '#99CCFF' #"153,204,255"
  deepblue    <- '#3333FF' #"51,51,255"
  lightpurple <- '#CC99FF' #"204,153,255"
  deeppurple  <- '#9933FF' #"153,51,255"
  encAP       <- '#FF0000' #"255,0,0"     # encode Active Promoter
  encPF       <- '#FF9999' #"255,153,153" # encode Promoter Flanking
  encSE       <- '#FFC800' #"255,200,0"   # encode Strong Enhancer
  actreg      <- '#D0EEC7' #"208,238,199" # '#FFEEB2' #"255,238,178" # encode low activity
  encWE       <- '#FFFF99' #"255,255,153" # encode Weak Enhancer/DNase only
  coreRed     <- '#FF4500' #"255,69,0"    # for core enhancer regions
  encIP       <- deeppurple #"225,0,225"   # encode inactive promoter
  encIPlite   <- lightpurple #"225,153,255" # a lighter color for flanking regions of inactive promoters
  myIEcore    <- deepred       # inactive enhancer core
  myIEflnk    <- lightred      # inactive enhancer flank
  encCTCF      <- '#0ABEFE' #10,190,254
  encCTCFdk    <- '#0A46FE' #10,70,254
  ELON.col     <- '#00B050' #0,176,80
  # my colors
  PAR.col      <- '#FF9999' # 255,153,153
  EAR.col      <- '#FFC800'
  PPR.col      <- '#CC99FF'
  EPR.col      <- '#CC99FF'
  PR.col       <- '#FFC6C6' # 255,198,198
  ER.col       <- '#FFE790' # 255,231,144
  RPS.col      <- '#FFFF99' # 255,255,153
  CTCF.col     <- '#A1E6FF' # 161,230,255
  PARC.col     <- '#FF0000' # 255,0,0
  EARC.col     <- '#FF4500' # 255,69,0
  PPRC.col     <- '#9933FF'
  EPRC.col     <- '#9933FF'
  PRC.col      <- '#FF9999' # 255,153,153
  ERC.col      <- '#FF9168' # 255,145,104
  CTCFCORE.col <- '#0ABEFE' # 10,190,254 other:'#0A46FE'
  ACTR.col     <- '#C2D69A' # 194,214,154 ENCODE "Low"
  SCR.col      <- '#808080' # 128,128,128
  HET.col      <- '#8A91D0' # 138,145,208
  TRS.col      <- '#008000' # 0,128,0
  ##
  colorizebed <- function (bed, name, color) {
    if(length(bed) >= 1) {
      mcols(bed)$itemRgb <- color
      mcols(bed)$name    <- name
      return(bed)
    } else {
      return(GRanges())
    }
  }

  ##
  ## Create empty GRanges to put annotations
  ##
  segmentations <- GRanges()
  ### BLOCK 1: Segmentations pertaining to the presence of Histone modifications
  if (HAVE_ACTIVE_MARKS) {
    segmentations <- c(segmentations,
                       colorizebed(EAR, '2_EAR', EAR.col),
                       colorizebed(PAR, '6_PAR', PAR.col))
    if (HAVE_PASSIVE_MARKS) {
      segmentations <- c(segmentations,
                         colorizebed(EPR, '4_EPR', EPR.col),
                         colorizebed(PPR, '8_PPR', PPR.col))
    }
  } else if (HAVE_PASSIVE_MARKS) {
    segmentations <- c(segmentations,
                       colorizebed(ER, '10_ER', ER.col),
                       colorizebed(PR, '12_PR', PR.col))
  } else if (HAVE_PROMARKS) {
    segmentations <- c(segmentations,
                       colorizebed(PR, '12_PR', PR.col))
  }


  ### BLOCK 2: Segmentations pertaining to the presence of open chromatin
  if (HAVE_OPEN_CHROMATIN) {
    if (HAVE_ACTIVE_MARKS) {
      segmentations <- c(segmentations,
                         colorizebed(EARC, '3_EARC', EARC.col),
                         colorizebed(PARC, '7_PARC', PARC.col))
      if (HAVE_PASSIVE_MARKS) {
        segmentations <- c(segmentations,
                           colorizebed(PPRC, '9_PPRC', PPRC.col),
                           colorizebed(EPRC, '5_EPRC', EPRC.col))
      }
    } else if (HAVE_PASSIVE_MARKS) {
      segmentations <- c(segmentations,
                         colorizebed(ERC, '11_ERC', ERC.col),
                         colorizebed(PRC, '13_PRC', PRC.col))
    } else if (HAVE_PROMARKS) {
      segmentations <- c(segmentations,
                         colorizebed(PRC, '13_PRC', PRC.col))
    }
    segmentations <- c(segmentations, colorizebed(RPS, '14_RPS', RPS.col))
  }

  ### CTCF segmentations
  if (HAVE_CTCF) {
    if (HAVE_OPEN_CHROMATIN) {
      segmentations <- c(segmentations, colorizebed(CTCFCORE, '19_CTCFC', CTCFCORE.col))
    }
    segmentations <- c(segmentations, colorizebed(CTCF, '18_CTCF', CTCF.col))
  }

  ### ACTR (active regions) segmentations
  if (HAVE_ACTIVE_BROAD) {
    # final cleanup of ACTR:
    ACTR <- subtractBed(active_region_foot, segmentations)
    segmentations <- c(segmentations,
                       colorizebed(ACTR, '1_ACTR', ACTR.col))
  }
  ### TRS (transcribed regions) segementations
  if (HAVE_TRANSCR_MARKS) {
    segmentations <- c(segmentations,
                        colorizebed(TRS, '17_TRS', TRS.col))
  }
  ### BLOCK 3: Segmentations pertaining to repressed chromatin
  #
  if (HAVE_REPR_MARKS) {
    segmentations <- c(segmentations,
                       colorizebed(SCR, '15_SCR', SCR.col))
  }
  #
  if (HAVE_HETC_MARKS) {
    segmentations <- c(segmentations,
                       colorizebed(HET, '16_HET', HET.col))
  }
  
      
  ##  
  ##
  ## FINAL OUTPUT:
  ##
  final_file_name <- paste0('../RSEGMENTATIONS/', CELLTYPE, '.SEGMENTATION.bed')
  system(command = paste0("rm -f ", final_file_name))
  segmentation_file <- file(final_file_name)
  open(segmentation_file, 'w')
  #writeLines("browser position chr19:51,304,932-51,404,939", segmentation_file)
  writeLines("browser position chr17:7,302,642-7,469,771", segmentation_file)
  export.bed(sort(segmentations), con=segmentation_file, trackLine=as(paste0('track name=\"', CELLTYPE, ' SEGMENTATIONS\" description=\"SEGMENTATION ON THE BASIS OF EPIGENETIC DATA FROM ', CELLTYPE, '\" itemRgb=\"On\"'), "BasicTrackLine"))
  close(segmentation_file)
  print(paste(CELLTYPE, "SEGMENTATION COMPLETE."))
  ##
  ##     **** END ****
  ##
}
