# file footprints.R
# R implementation of footprints.py
library(GenomicRanges)
library(rtracklayer)
library(stringr)
library(parallel)
source('/home/rstudio/data/R/bedtools.R')
source('/home/rstudio/data/R/footprint.R')
#source('/Users/hazelettd/Desktop/projects/mydockerbuild/footprint/data/R/bedtools.R')
#source('/Users/hazelettd/Desktop/projects/mydockerbuild/footprint/data/R/footprint.R')

sessionInfo()
setwd(dir = '/home/rstudio/data/bed')
#setwd(dir = '/Users/hazelettd/Desktop/projects/mydockerbuild/sandbox1/data/bed')
#setwd(dir = '/Users/hazelettd/Desktop/projects/segChromDiff/bed')



#MANIFEST  <- read.table('fivemark.manifest.txt', header = TRUE, stringsAsFactors = FALSE)
#MANIFEST  <- read.table('manifest.tier1.txt', header = TRUE, stringsAsFactors = FALSE)
MANIFEST  <- read.table('manifest.txt', header = TRUE, stringsAsFactors = FALSE)

CELLTYPES <- unique(MANIFEST$SAMPLE)

ENV_CELLTYPE <- 'ALL'

if (ENV_CELLTYPE=='ALL') {
  num.avail.cores <- detectCores()
  system.time(msgs <- mclapply(X = CELLTYPES, FUN = footprint, MANIFEST=MANIFEST, mc.cores = num.avail.cores))
} else {
  footprint(CELLTYPE = ENV_CELLTYPE, MANIFEST = MANIFEST)
}
