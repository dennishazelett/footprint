#!/bin/sh
mkdir -p ~/data/SEGMENTATIONS
date
cut -f1 ~/data/config/manifest.txt | grep -v ALL | grep -v SAMPLE | sort -u > ~/data/config/cells.txt
parallel -a ~/data/config/cells.txt -P 3 python ~/data/python/footprints.py {1}
date
