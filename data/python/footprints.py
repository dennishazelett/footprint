# file footprints.py
# rules based classification of variants based on 
# histone modifications and transcription factor ChIPseq

import sys, string, gzip, os, subprocess
from pybedtools import *

## import the manifest of files containing peak calls and
## extract sample and mark metadata

class bed_meta_data:

    def __init__(self, manifest_line):
        manifest_fields = string.split(manifest_line)
        self.SAMPLE     = manifest_fields[0]
        self.MARK       = string.upper(manifest_fields[1])
        self.SRC        = manifest_fields[2]
        self.BUILD      = manifest_fields[3]
        self.FILE       = string.strip(manifest_fields[4])

    def get_sample(self):
        return self.SAMPLE

    def get_mark(self):
        return self.MARK

    def get_source(self):
        return self.SRC

    def get_build(self):
        return self.BUILD

    def get_file(self):
        return self.FILE
    


def convert_genome(track, genome1, genome2, asBedTool=True):
    '''This is a wrapper function for the liftover utility in pybedtools. It maps files to the \
    relevant liftover chains in this annotation pipeline in the ~/data/liftover directory.'''
    # if asBedTool==False, assume the track argument references a file name, and read it in
    # using pybedtools module.
    basedir = os.getcwd()
    if not asBedTool:
        os.chdir('/home/daedelus/data/bed')
        track = BedTool(track)
    # before performing liftover set cwd to liftover chain directory, then liftover with
    # pybedtools.
    os.chdir('/home/daedelus/data/liftover')
    #set_bedtools_path('/home/daedelus/data/liftover') # from pybedtools module
    if genome1=='hg18':
        if genome2=='hg19':
            track_new = track.liftover(chainfile='hg18ToHg19.over.chain.gz')
        elif genome2=='hg38':
            track_new = track.liftover(chainfile='hg18ToHg38.over.chain.gz')
        else:
            raise NameError("Could not locate appropriate chainfile. %s-->%s" % (genome1, genome2,))
    elif genome1=='hg19':
        if genome2=='hg38':
            track_new = track.liftover(chainfile='hg19ToHg38.over.chain.gz')
        else:
            raise NameError("Could not locate appropriate chainfile. %s-->%s" % (genome1, genome2,))
    else:
        raise NameError("Could not locate appropriate chainfile. %s-->%s" % (genome1, genome2,))
    # reset cwd to basedir
    os.chdir(basedir)
    #set_bedtools_path(basedir) # from pybedtools module
    return track_new

def file_name(sample, mark, manifest):
    list_of_names = []
    for each in manifest:
        if each['SAMPLE']==sample and each['MARK']==mark:
            list_of_names.append(each['FILE'])
    return list_of_names

if __name__=='__main__':
    ## Note the following can be overridden at call by supplying GENOME=hg18 in sys.argv (for example)
    BUILD = 'hg19'
    CELLTYPE = sys.argv[1]
    print "CELLTYPE = %s" % CELLTYPE
    manifest_text = open('/home/daedelus/data/config/manifest.txt', 'r').readlines()
    os.chdir('/home/daedelus/data/bed')
    MANIFEST = [bed_meta_data(line) for line in manifest_text]
    
    tmpdir = '/home/daedelus/data/bed/tmp/'
    ##
    ## ENHANCER CLASSIFICATION
    ##
    ##
    promoter_marks_defined   = ['K4M3', 'H3K4ME3']             
    regulatory_marks_passive = ['K4M1', 'H3K4ME1', 'K4M2', 'H3K4ME2']
    regulatory_marks_active  = ['K27AC', 'H3K27AC', 'K9AC', 'H3K9AC']
    enhancer_marks_core      = ['DHS', 'DNASEI', 'DNASE', 'FAIRE', 'HOT', 'TF', 'TROUGH']
    transcription_factor     = ['FOXA1', 'NKX31', 'NKX3-1', 'AR', 'AROR', 'TCF7L1', 'TAL1', 'CEBPB',
                                'ATF1', 'NR3C1', 'TEAD1', 'ZBTB33', 'SUZ12', 'MYC', 'MAX']
    TSS                      = ['TSS']

    
    ## "set" function returns unique names, but has to be coerced back to list type.
    all_regulatory_marks     = list(set(promoter_marks_defined +
                                        regulatory_marks_active +
                                        regulatory_marks_passive))

    open_chromatin           = list(set(enhancer_marks_core + transcription_factor))

    beds_cell = []

    nullbed = [('chr1', 1, 2)]

    for each_file in MANIFEST:
        if each_file.get_mark() in all_regulatory_marks and each_file.get_sample()==CELLTYPE:
            beds_cell.append(BedTool(each_file.get_file()))

    bedfiles = [bedfile.sort().fn for bedfile in beds_cell]
    if len(bedfiles) > 1:
        # this might be wrong
        regulatory_footprint = BedTool().multi_intersect(i=bedfiles).sort().merge()

    elif len(bedfiles)==1:
        regulatory_footprint = BedTool(bedfiles[0])

    else:
        regulatory_footprint = BedTool(nullbed)
        
    regulatory_footprint.saveas('tmp/%s.footprint.bed' % CELLTYPE,
                                trackline='track name=\'%s FOOTPRINT\' description=\'FOOTPRINT SEGMENTATION BASED ON HISTONE MARKS [%s]\'' % (CELLTYPE, CELLTYPE,))
    subprocess.call('sed -i \'1ibrowser position chr19:51338268-51371603\' tmp/%s.footprint.bed' % CELLTYPE, shell=True)
    # cleanup
    #del(beds_cell)
    
    ## Determine whether it is possible to annotate promoters
    HAVE_PROMARKS = False
    promoter_beds = []
    for each_file in MANIFEST:

        if each_file.get_mark() in promoter_marks_defined and each_file.get_sample()==CELLTYPE:
            HAVE_PROMARKS = True
            promoter_beds.append(BedTool(each_file.get_file()))
            
        else:
            continue

    # Generate list of promoters based upon available promoters
        
    if HAVE_PROMARKS:

        if len(promoter_beds) > 1: # multi_intersect does not work with 1 bed as input
            promoter_footprint = BedTool().multi_intersect(i=[each_bed.sort().merge().fn for each_bed in promoter_beds])
        elif len(promoter_beds) == 1:
            promoter_footprint = promoter_beds[0]

    else:

        for each_file in MANIFEST:
            if each_file.get_mark()=='TSS' and each_file.get_sample()=='ALL':
                promoter_footprint = BedTool(each_file.get_file()).sort().merge()
                break
            else:
                continue
 
    #print "%s completed promoters" % CELLTYPE

    ##
    ## Determine whether enhancers can be annotated
    ##
    HAVE_ENHANCER_MARKS = False
    enhancer_beds = []
    enhancer_marks_all = regulatory_marks_active + regulatory_marks_passive
    for each_file in MANIFEST:
        if each_file.get_mark() in enhancer_marks_all and each_file.get_sample()==CELLTYPE:
            HAVE_ENHANCER_MARKS = True
            enhancer_beds.append(BedTool(each_file.get_file()))
        else:
            continue

    HAVE_PASSIVE_MARKS = False
    passive_beds = []
    passive_marks = regulatory_marks_passive
    for each_file in MANIFEST:
        if each_file.get_mark() in passive_marks and each_file.get_sample()==CELLTYPE:
            HAVE_PASSIVE_MARKS = True
            enhancer_beds.append(BedTool(each_file.get_file()))
        else:
            continue

        # Generate list of enhancer footprint based on enhancer marks

    if HAVE_ENHANCER_MARKS:
        if len(enhancer_beds) > 1:
            enhancer_footprint = BedTool().multi_intersect(i=[each_bed.sort().merge().fn for each_bed in enhancer_beds]).sort().merge()
        else:
            enhancer_footprint = BedTool(enhancer_beds[0]).sort().merge()
    else:
        enhancer_footprint = BedTool(nullbed)


    # update enhancer footprint: remove promoters
    if not HAVE_PROMARKS:
        if HAVE_ENHANCER_MARKS:
            # use aggregate enhancer marks to discard non-epigenitcally marked promoters
            promoter_footprint = promoter_footprint.intersect(enhancer_footprint, wa=True).sort().merge()
    # then subtract these from the enhancer footprint
    enhancer_footprint = enhancer_footprint.subtract(promoter_footprint).sort().merge()
        
    #print "%s completed enhancers" % CELLTYPE
    ##
    ## ACTIVE FOOTPRINT
    ##
    HAVE_ACTIVE_MARKS = False
    active_beds = []
    for each_file in MANIFEST:
        if each_file.get_mark() in regulatory_marks_active and each_file.get_sample()==CELLTYPE:
            HAVE_ACTIVE_MARKS = True
            active_beds.append(BedTool(each_file.get_file()))
        else:
            continue

    # Generate active footprint

    if HAVE_ACTIVE_MARKS:
        if len(active_beds) > 1:
            active_footprint = BedTool().multi_intersect(i=[each_bed.sort().merge().fn for each_bed in active_beds]).sort().merge()
        else:
            active_footprint = BedTool(active_beds[0]).sort().merge()
    else:
        active_footprint = BedTool(nullbed)
        

    ##
    ## Identify and segregate active promoters and enhancers
    ##
    if HAVE_ACTIVE_MARKS:
        active_promoter_footprint = promoter_footprint.intersect(active_footprint, wa=True).sort().merge()
        poised_promoter_footprint = promoter_footprint.subtract(active_footprint, A=True).sort().merge()
    else:
        active_promoter_footprint, poised_promoter_footprint = BedTool(nullbed), BedTool(nullbed)

    ## segregate
    if HAVE_ACTIVE_MARKS:
        active_enhancer_footprint = enhancer_footprint.intersect(active_footprint, wa=True).sort().merge().subtract(active_promoter_footprint).sort().merge()
        #remove active enhancers from poised
        if HAVE_PASSIVE_MARKS:
            poised_enhancer_footprint = enhancer_footprint.subtract(active_enhancer_footprint, A=True).sort().merge().subtract(poised_promoter_footprint).sort().merge()
        else:
            poised_promoter_footprint = BedTool(nullbed)
    else:
        active_enhancer_footprint, poised_enhancer_footprint = BedTool(nullbed), BedTool(nullbed)

    #print "%s completed active marks" % CELLTYPE

    ##
    ## Identify open chromatin regions
    ## (DEFINITION INCLUDES TF BINDING)
    ##
    HAVE_OPEN_CHROMATIN = False
    open_chromatin_beds = []
    for each_file in MANIFEST:
        if each_file.get_mark() in open_chromatin and each_file.get_sample()==CELLTYPE:
            HAVE_OPEN_CHROMATIN = True
            open_chromatin_beds.append(BedTool(each_file.get_file()))
        else:
            continue

    # Generate open_chromatin footprint:

    if HAVE_OPEN_CHROMATIN:
        if len(open_chromatin_beds) > 1:
            open_chromatin_footprint = BedTool().multi_intersect(i=[each_bed.sort().merge().fn for each_bed in open_chromatin_beds]).sort().merge()
        else:
            open_chromatin_footprint = BedTool(open_chromatin_beds[0]).sort().merge()
    else:
        open_chromatin_footprint = BedTool(nullbed)


    #print "%s completed open chromatin and TFs" % CELLTYPE
    ##
    ## identify enhancer cores, promoter cores, and putative regulatory sites based on open chromatin
    ##

    if HAVE_OPEN_CHROMATIN:
        if HAVE_ACTIVE_MARKS:
            active_promoter_core_footprint = open_chromatin_footprint.intersect(active_promoter_footprint, wa=True).sort().merge()
            active_enhancer_core_footprint = open_chromatin_footprint.intersect(active_enhancer_footprint, wa=True).sort().merge()
            if HAVE_PASSIVE_MARKS:
                poised_promoter_core_footprint = open_chromatin_footprint.intersect(poised_promoter_footprint, wa=True).sort().merge()
                poised_enhancer_core_footprint = open_chromatin_footprint.intersect(poised_enhancer_footprint, wa=True).sort().merge()
                poised_enhancer_core_footprint = poised_enhancer_core_footprint.subtract(poised_promoter_core_footprint).sort().merge()
            putative_regulatory_sites      = open_chromatin_footprint.subtract(regulatory_footprint,        A=True).sort().merge()
        elif not HAVE_ENHANCER_MARKS:
            # enhancer marks are the most basic annotation data, however if they are lacking it \
            # is still possible to annotate from chromatin marks alone
            if HAVE_PROMARKS:
                promoter_core_footprint   = open_chromatin_footprint.intersect(promoter_footprint, wa=True).sort().merge()
                promoter_footprint        = promoter_footprint.subtract(promoter_core_footprint).sort().merge()
                putative_regulatory_sites = open_chromatin_footprint.subtract(promoter_core_footprint).sort().merge()
            else:
                putative_regulatory_sites = open_chromatin_footprint.sort().merge()            
        else:
            # assuming general enhancer marks but not active are available, generate appropriate segmentations
            enhancer_core_footprint        = open_chromatin_footprint.intersect(enhancer_footprint, wa=True).sort().merge()
            promoter_core_footprint        = open_chromatin_footprint.intersect(promoter_footprint, wa=True).sort().merge()
            putative_regulatory_sites      = open_chromatin_footprint.subtract(enhancer_core_footprint, A=True).sort().merge()
            putative_regulatory_sites      = putative_regulatory_sites.subtract(promoter_core_footprint, A=True).sort().merge()
    #print "%s completed core definitions" % CELLTYPE

    ##
    ## final refinements, subtract core regions from enhancers and promoters and rename
    ##
    if not HAVE_ACTIVE_MARKS:
        PR = promoter_footprint
        ER = enhancer_footprint
        PR.saveas( 'tmp/%s.PR.bed' % CELLTYPE)
        ER.saveas( 'tmp/%s.ER.bed' % CELLTYPE)
        if HAVE_OPEN_CHROMATIN:
            if HAVE_ENHANCER_MARKS:
                ERC = enhancer_core_footprint
                PRC = promoter_core_footprint
                ERC.saveas( 'tmp/%s.ERC.bed' % CELLTYPE)
                PRC.saveas( 'tmp/%s.PRC.bed' % CELLTYPE)
            elif HAVE_PROMARKS:
                PRC = promoter_core_footprint
                PRC.saveas( 'tmp/%s.PRC.bed' % CELLTYPE)
            RPS  = putative_regulatory_sites
            RPS.saveas( 'tmp/%s.RPS.bed' % CELLTYPE)

    else: ## i.e. if HAVE_ACTIVE_MARKS

        # complete this section only if open chromatin available
        if HAVE_OPEN_CHROMATIN:
            PAR  = active_promoter_footprint.subtract(open_chromatin_footprint).sort().merge()
            EAR  = active_enhancer_footprint.subtract(open_chromatin_footprint).sort().merge()
            if HAVE_PASSIVE_MARKS:
                PPR  = poised_promoter_footprint.subtract(open_chromatin_footprint).sort().merge()
                EPR  = poised_enhancer_footprint.subtract(open_chromatin_footprint).sort().merge()
                PPRC = poised_promoter_core_footprint
                EPRC = poised_enhancer_core_footprint
                PPRC.saveas('tmp/%s.PPRC.bed' % CELLTYPE)
                EPRC.saveas('tmp/%s.EPRC.bed' % CELLTYPE)
            EARC = active_enhancer_core_footprint.subtract(promoter_footprint).sort().merge()
            PARC = active_promoter_core_footprint.subtract(EARC).sort().merge()
            RPS  = putative_regulatory_sites
            EARC.saveas('tmp/%s.EARC.bed' % CELLTYPE)
            PARC.saveas('tmp/%s.PARC.bed' % CELLTYPE)
            RPS.saveas( 'tmp/%s.RPS.bed'  % CELLTYPE)
        else:
            PAR  = active_promoter_footprint
            EAR  = active_enhancer_footprint
            if HAVE_PASSIVE_MARKS:
                PPR  = poised_promoter_footprint
                EPR  = poised_enhancer_footprint
        PAR.saveas( 'tmp/%s.PAR.bed'  % CELLTYPE)
        EAR.saveas( 'tmp/%s.EAR.bed'  % CELLTYPE)
        if HAVE_OPEN_CHROMATIN:
            EPR.saveas( 'tmp/%s.EPR.bed'  % CELLTYPE)
            PPR.saveas( 'tmp/%s.PPR.bed'  % CELLTYPE)
                
    #print "%s completed filesave" % CELLTYPE
    
    subprocess.call('rm -f tmp/%s.SEGMENTATION.bed; echo "browser position chr19:51,304,932-51,404,939" > tmp/tmp.%s.bed; echo "track name=\'%s [SEGMENTATION]\' description=\'EMPIRICAL SEGMENTATION FROM HISTONE MARKS [%s]\' itemRgb=\'On\'" >> tmp/tmp.%s.bed' % (CELLTYPE, CELLTYPE, CELLTYPE, CELLTYPE, CELLTYPE,), shell=True)
    # define colors in RGB colorspace
    lightred    = "255,204,204"
    deepred     = "155,0,0"
    lightgreen  = "229,255,204"
    deepgreen   = "0,155,0"
    lightorange = "255,229,204"
    deeporange  = "255,128,0"
    lightyellow = "225,225,51"
    lightblue   = "153,204,255"
    deepblue    = "51,51,255"
    lightpurple = "204,153,255"
    deeppurple  = "153,51,255"
    encAP       = "255,0,0"     # encode Active Promoter
    encPF       = "255,153,153" # encode Promoter Flanking
    encSE       = "255,200,0"   # encode Strong Enhancer
    encWE       = "255,255,153" # encode Weak Enhancer/DNase only
    coreRed     = "255,69,0"    # for core enhancer regions
    encIP       = "225,0,225"   # encode inactive promoter
    encIPlite   = "225,153,255" # a lighter color for flanking regions of inactive promoters
    myIEcore    = deepred       # inactive enhancer core
    myIEflnk    = lightred      # inactive enhancer flank
        
    if HAVE_ACTIVE_MARKS: # if no enhancer marks, annotation may not be possible
        awkscripts  = ['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"EAR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.EAR.bed  >> tmp/%s.SEGMENTS.bed' % (encSE,     CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PAR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PAR.bed  >> tmp/%s.SEGMENTS.bed' % (encPF,     CELLTYPE, CELLTYPE,)]
        if HAVE_PASSIVE_MARKS:
            awkscripts.extend(['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"EPR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.EPR.bed  >> tmp/%s.SEGMENTS.bed' % (myIEflnk,  CELLTYPE, CELLTYPE,),
                               'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PPR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PPR.bed  >> tmp/%s.SEGMENTS.bed' % (encIPlite, CELLTYPE, CELLTYPE,)])
    else:
        awkscripts = ['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"ER\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.ER.bed  >> tmp/%s.SEGMENTS.bed' %   (encSE,  CELLTYPE, CELLTYPE,),
                      'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PR.bed  >> tmp/%s.SEGMENTS.bed' %   (encPF,     CELLTYPE, CELLTYPE,)]
    if HAVE_OPEN_CHROMATIN:
        if HAVE_ACTIVE_MARKS:
            awkscripts.extend(['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"EARC\", 0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.EARC.bed >> tmp/%s.SEGMENTS.bed' % (coreRed, CELLTYPE, CELLTYPE,),
                               'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PARC\", 0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PARC.bed >> tmp/%s.SEGMENTS.bed' % (encAP,   CELLTYPE, CELLTYPE,)])
            if HAVE_PASSIVE_MARKS:
                awkscripts.extend(['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PPRC\", 0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PPRC.bed >> tmp/%s.SEGMENTS.bed' % (encIP,   CELLTYPE, CELLTYPE,),
                                   'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"EPRC\", 0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.EPRC.bed >> tmp/%s.SEGMENTS.bed' % (deepred, CELLTYPE, CELLTYPE,)])
        else: # not HAVE_ACTIVE_MARKS
            if HAVE_ENHANCER_MARKS:
                awkscripts.extend(['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"ERC\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.ERC.bed  >> tmp/%s.SEGMENTS.bed' % (encSE,   CELLTYPE, CELLTYPE,),
                                   'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PRC\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PRC.bed  >> tmp/%s.SEGMENTS.bed' % (encAP,   CELLTYPE, CELLTYPE,)])
            elif HAVE_PROMARKS:
                awkscripts.extend(['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PRC\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PRC.bed  >> tmp/%s.SEGMENTS.bed' % (encAP,   CELLTYPE, CELLTYPE,)])
        awkscripts.extend(['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"RPS\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.RPS.bed  >> tmp/%s.SEGMENTS.bed' % (encWE,   CELLTYPE, CELLTYPE,)])
                              
    awkscripts.append('grep -v \'_\' tmp/%s.SEGMENTS.bed | sortBed -i - | intersectBed -a - -b hg19.bed | sortBed -i - >> tmp/tmp.%s.bed; rm -f tmp/%s.SEGMENTS.bed; rm -f tmp/%s.*.bed; mv tmp/tmp.%s.bed ../SEGMENTATIONS/%s.SEGMENTATION.bed' % (CELLTYPE, CELLTYPE, CELLTYPE, CELLTYPE, CELLTYPE, CELLTYPE,))
    for command in awkscripts:
        # print command
        subprocess.call(command, shell=True)

    #regulatory_footprint.saveas('tmp/%s.footprint.bed' % CELLTYPE,
    #                            trackline='track name=\'%s FOOTPRINT\' description=\'FOOTPRINT SEGMENTATION BASED ON HISTONE MARKS [%s]\'' % (CELLTYPE, CELLTYPE,))
    #subprocess.call('sed -i \'1ibrowser position chr19:51338268-51371603\' tmp/%s.footprint.bed' % CELLTYPE, shell=True)

    print "SEGMENTATION COMPLETE."
