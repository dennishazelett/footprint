# file annotate.py
# rules based classification of variants based on 
# histone modifications and transcription factor ChIPseq

import sys, string, gzip, os, subprocess
from pybedtools import *

## import the manifest of files containing peak calls and
## extract sample and mark metadata

class bed_meta_data:

    def __init__(self, manifest_line):
        manifest_fields = string.split(manifest_line)
        self.SAMPLE     = manifest_fields[0]
        self.MARK       = manifest_fields[1]
        self.SRC        = manifest_fields[2]
        self.BUILD      = manifest_fields[3]
        self.FILE       = string.strip(manifest_fields[4])

    def get_sample(self):
        return self.SAMPLE

    def get_mark(self):
        return self.MARK

    def get_source(self):
        return self.SRC

    def get_build(self):
        return self.BUILD

    def get_file(self):
        return self.FILE
    


def convert_genome(track, genome1, genome2, asBedTool=True):
    '''This is a wrapper function for the liftover utility in pybedtools. It maps files to the \
    relevant liftover chains in this annotation pipeline in the ~/data/liftover directory.'''
    # if asBedTool==False, assume the track argument references a file name, and read it in
    # using pybedtools module.
    basedir = os.getcwd()
    if not asBedTool:
        os.chdir('/home/daedelus/data/bed')
        track = BedTool(track)
    # before performing liftover set cwd to liftover chain directory, then liftover with
    # pybedtools.
    os.chdir('/home/daedelus/data/liftover')
    #set_bedtools_path('/home/daedelus/data/liftover') # from pybedtools module
    if genome1=='hg18':
        if genome2=='hg19':
            track_new = track.liftover(chainfile='hg18ToHg19.over.chain.gz')
        elif genome2=='hg38':
            track_new = track.liftover(chainfile='hg18ToHg38.over.chain.gz')
        else:
            raise NameError("Could not locate appropriate chainfile. %s-->%s" % (genome1, genome2,))
    elif genome1=='hg19':
        if genome2=='hg38':
            track_new = track.liftover(chainfile='hg19ToHg38.over.chain.gz')
        else:
            raise NameError("Could not locate appropriate chainfile. %s-->%s" % (genome1, genome2,))
    else:
        raise NameError("Could not locate appropriate chainfile. %s-->%s" % (genome1, genome2,))
    # reset cwd to basedir
    os.chdir(basedir)
    #set_bedtools_path(basedir) # from pybedtools module
    return track_new

def file_name(sample, mark, manifest):
    list_of_names = []
    for each in manifest:
        if each['SAMPLE']==sample and each['MARK']==mark:
            list_of_names.append(each['FILE'])
    return list_of_names
            
if __name__=='__main__':
    ## Note the following can be overridden at call by supplying GENOME=hg18 in sys.argv (for example)
    BUILD = 'hg19'
    CELLTYPE = sys.argv[1]
    print "CELLTYPE = %s" % CELLTYPE
    manifest_text = open('/home/daedelus/data/bed/manifest.txt', 'r').readlines()
    MANIFEST = [bed_meta_data(line) for line in manifest_text]
    
    tmpdir = '/home/daedelus/data/bed/tmp/'
    ##
    ## ENHANCER CLASSIFICATION
    ##
    ## 
    promoter_marks_defined   = ['K4M3']             
    regulatory_marks_passive = ['K4M1']
    regulatory_marks_active  = ['K27AC', 'K9AC']
    enhancer_marks_core      = ['DHS', 'TF', 'HOT']
    open_chromatin           = ['DHS', 'FAIRE', 'TF']
    TSS                      = ['TSS']
    ## "set" function returns unique names, but has to be coerced back to list type.
    available_marks = list(set([item.get_mark() for item in MANIFEST]))

    # Initialize empty collector BedTool objects for every possible segmentation track:
    nulltrack = [('chr1', 0, 1)]
    PRS  = BedTool(nulltrack) # PUTATIVE REGULATORY REGION
    EAR  = BedTool(nulltrack) # ENHANCER ACTIVE REGION
    EPR  = BedTool(nulltrack) # ENHANCER POISED REGION
    EARC = BedTool(nulltrack) # ENHANCER ACTIVE REGION CORE
    EPRC = BedTool(nulltrack) # ENHANCER POISED REGION CORE
    PAR  = BedTool(nulltrack) # PROMOTER ACTIVE REGION
    PPR  = BedTool(nulltrack) # PROMOTER POISED REGION
    PARC = BedTool(nulltrack) # PROMOTER ACTIVE REGION CORE
    PPRC = BedTool(nulltrack) # PROMOTER POISED REGION CORE
    MIRT = BedTool(nulltrack) # POTENTIAL CELL TYPE MIR TARGET
    
    if available_marks:

        ## LOOP THROUGH MARKS
        ## UPDATING SEGMENTATIONS AS APPROPRIATE
        os.chdir('/home/daedelus/data/bed/')

        ## Define TSS

        for bed_file in MANIFEST:
            if bed_file.get_mark() in TSS:
                tssbed = BedTool(bed_file.get_file()).sort().merge()
        
        ## RULE for case passive reglatory mark
        for mark in regulatory_marks_passive:
            for bed_file in MANIFEST:
                MARK       = bed_file.get_mark()
                bed_source = bed_file.get_source()

                ## check that bed_file corresponds to CELLTYPE
                if bed_file.get_sample() in [CELLTYPE, 'ALL'] and MARK==mark:
                    ## enforce genome build and read into bed file
                    bed = BedTool(bed_file.get_file()).sort().merge()
                    print '%5s mark available for %s' % (MARK, CELLTYPE,)
                    if bed_file.get_build() != BUILD:
                        bed = convert_genome(bed, bed_file.get_build(), BUILD)
                        bed.head()
                        print "Converted %s from %s to %s." % (MARK, bed_file.get_build(), BUILD,)
                    # application of the rule
                    # all enhancers include poised with mark, exclude tss regions
                    # EPR  = EPR.cat(bed).intersect(tssbed, v=True).sort().merge()
                    EPR  = bed.subtract(tssbed, A=True).sort().merge().cat(EPR)
                    # all promoters include poised with mark, overlap tss regions
                    # PPR  = PPR.cat(bed).intersect(tssbed, u=True, wa=True).sort().merge()
                    PPR  = bed.intersect(tssbed, u=True, wa=True).sort().merge().cat(PPR)
                else:
                    continue


        ## RULE for promoter marks
        for mark in promoter_marks_defined:

            for bed_file in MANIFEST:

                MARK       = bed_file.get_mark()
                bed_source = bed_file.get_source()

                ## check that bed_file corresponds to CELLTYPE
                if bed_file.get_sample() in [CELLTYPE, 'ALL'] and MARK==mark:
                    ## enforce genome build and read into bed file
                    bed = BedTool(bed_file.get_file()).sort().merge()
                    print '%5s mark available for %s' % (MARK, CELLTYPE,)
                    if bed_file.get_build() != BUILD:
                        bed = convert_genome(bed, bed_file.get_build(), BUILD)
                        bed.head()
                        print "Converted %s from %s to %s." % (MARK, bed_file.get_build(), BUILD,)
                    ## application of the rule
                    # add promoter marks to the full set of promoter segments
                    PPR = bed.cat(PPR).merge()
                    ## also remove new promoter regions from enhancers
                    EPR = EPR.subtract(bed, A=True).sort().merge()
                    ## expand the definition of tss to include the promoter mark
                    tssbed = tssbed.cat(bed).merge()
                else:
                    continue

        # segregate putative promoters from enhancers        
        # populate active promoters and enhancers prior to filtering
        PAR = PPR
        EAR = EPR
                
        ## RULE for case active marks
        for mark in regulatory_marks_active:

            for bed_file in MANIFEST:

                MARK       = bed_file.get_mark()
                bed_source = bed_file.get_source()

                ## check that bed_file corresponds to CELLTYPE
                if bed_file.get_sample()==CELLTYPE and MARK==mark:
                    ## enforce genome build and read into bed file
                    bed = BedTool(bed_file.get_file()).sort().merge()
                    print '%5s mark available for %s' % (MARK, CELLTYPE,)
                    if bed_file.get_build() != BUILD:
                        bed = convert_genome(bed, bed_file.get_build(), BUILD)
                        bed.head()
                        print "Converted %s from %s to %s." % (MARK, bed_file.get_build(), BUILD,)
                    ## application of the rule
                    #
                    activetss = bed.intersect(tssbed, wa=True).sort().merge() # active prom region
                    active_nc = bed.subtract(tssbed, A=True).sort().merge()   # active non-coding (non-tss)
                    # subtract active (tss) regions from poised
                    PPR  = PPR.subtract(activetss, A=True).sort().merge()
                    # define par as ppr that overlap active mark (that overlap tss)
                    PAR  = PAR.subtract(PPR, A=True).sort().merge()
                    PAR  = activetss.cat(PAR).merge()
                    #PAR  = 
                    #PAR  = PAR.subtract(PPR, A=True).sort().merge()
                    # subtract EAR from the poised set
                    EPR  = EPR.subtract(active_nc, A=True).sort().merge()
                    #EPR  = EPR.subtract(PPR, A=True).sort().merge()
                    # now, add active enhancer and subtract active promoters from the active enhancer set:
                    #EAR  = active_nc.cat(EAR).sort().merge()
                    EAR  = EAR.subtract(EPR, A=True).sort().merge()
                    EAR  = active_nc.cat(EAR).merge()
                else:
                    continue
                    
        for mark in open_chromatin:

            for bed_file in MANIFEST:

                MARK       = bed_file.get_mark()
                bed_source = bed_file.get_source()

                ## check that bed_file corresponds to CELLTYPE
                if bed_file.get_sample() in [CELLTYPE, 'ALL'] and MARK==mark:
                    ## enforce genome build and read into bed file
                    bed = BedTool(bed_file.get_file()).sort().merge()
                    print '%5s mark available for %s' % (MARK, CELLTYPE,)
                    if bed_file.get_build() != BUILD:
                        bed = convert_genome(bed, bed_file.get_build(), BUILD)
                        bed.head()
                        print "Converted %s from %s to %s." % (MARK, bed_file.get_build(), BUILD,)
                    ## application of the rule
                    PRS  = bed.cat(PRS)
                    EARC = EARC.cat(bed.intersect(EAR, wa=True).sort().merge())
                    EPRC = EPRC.cat(bed.intersect(EPR, wa=True).sort().merge())
                    PARC = PARC.cat(bed.intersect(PAR, wa=True).sort().merge())
                    PPRC = PPRC.cat(bed.intersect(PPR, wa=True).sort().merge())

                else:
                    continue

                
        ## REFINE SEGMENTATION CLASSES ACCORDING TO HIERARCHICAL RELATIONSHIP
        ## WITH ALL OTHER SEGMENTATION TRACKS.

        ## First step, identify PRS that have not been classified as enhancer

        PRS  = PRS.subtract(EAR, A=True).sort().merge()
        PRS  = PRS.subtract(PAR, A=True).sort().merge()
        PRS  = PRS.subtract(EPR, A=True).sort().merge()
        PRS  = PRS.subtract(PPR, A=True).sort().merge()
        
        ## Second step, remove active annotations from poised
        # already DONE!
        #EPR  = EPR.intersect(EAR, v=True).sort().merge()
        #EPRC = EPRC.intersect(EARC, v=True).sort().merge()
        #PPR  = PPR.intersect(PAR, v=True).sort().merge()
        #PPRC = PPRC.intersect(PARC, v=True).sort().merge()

        ## Fourth step, remove enhancers from promoters

        #PAR  = PAR.intersect(EAR, v=True).sort().merge()
        #PARC = PARC.intersect(EARC, v=True).sort().merge()
        #PPR  = PPR.intersect(EPR, v=True).sort().merge()
        #PPRC = PPRC.intersect(EPRC, v=True).sort().merge()

        ## Third step, remove promoters (defined by tssbed) from enhancers

        #EAR  = EAR.intersect(tssbed, v=True).sort().merge()
        #EPR  = EPR.intersect(tssbed, v=True).sort().merge()
        #EPRC = EPRC.intersect(tssbed, v=True).sort().merge()

        ## Fifth step, save each segmentation track to a file

        EAR.subtract(EARC).saveas('tmp/%s.EAR.bed'   % CELLTYPE)
        EPR.subtract(EPRC).saveas('tmp/%s.EPR.bed'   % CELLTYPE)
        EARC.saveas('tmp/%s.EARC.bed' % CELLTYPE)
        EPRC.saveas('tmp/%s.EPRC.bed' % CELLTYPE)
        PAR.subtract(PARC).saveas('tmp/%s.PAR.bed'   % CELLTYPE)
        PPR.subtract(PPRC).saveas('tmp/%s.PPR.bed'   % CELLTYPE)
        PARC.saveas('tmp/%s.PARC.bed' % CELLTYPE)
        PPRC.saveas('tmp/%s.PPRC.bed' % CELLTYPE)
        PRS.saveas('tmp/%s.PRS.bed'   % CELLTYPE)
        
        ## 6th step, add colors to segmentation and concatenate into a single bed file with header for browser display
        subprocess.call('rm -f tmp/%s.SEGMENTATION.bed; echo "browser position chr19:51338268-51371603" > tmp/tmp.bed; echo "track name=\'%s SEGMENTATION\' description=\'SEGMENTATION BASED ON HISTONE MARKS [%s]\' itemRgb=\'On\'" >> tmp/tmp.bed' % (CELLTYPE, CELLTYPE, CELLTYPE,), shell=True)
        # define colors in RGB colorspace
        lightred    = "255,204,204"
        deepred     = "155,0,0"
        lightgreen  = "229,255,204"
        deepgreen   = "0,155,0"
        lightorange = "255,229,204"
        deeporange  = "255,128,0"
        lightyellow = "225,225,0"
        lightgrey   = "210,210,210"
        lightblue   = "51,153,255"
        awkscripts  = ['awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"EAR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.EAR.bed  >> tmp/%s.SEGMENTS.bed' % (lightred,    CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"EPR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.EPR.bed  >> tmp/%s.SEGMENTS.bed' % (lightorange, CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PAR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PAR.bed  >> tmp/%s.SEGMENTS.bed' % (lightgreen,  CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PPR\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PPR.bed  >> tmp/%s.SEGMENTS.bed' % (lightyellow, CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"EARC\", 0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.EARC.bed >> tmp/%s.SEGMENTS.bed' % (deepred,     CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"EPRC\", 0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.EPRC.bed >> tmp/%s.SEGMENTS.bed' % (deeporange,  CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PARC\", 0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PARC.bed >> tmp/%s.SEGMENTS.bed' % (deepgreen,   CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PPRC\", 0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PPRC.bed >> tmp/%s.SEGMENTS.bed' % (lightgrey,   CELLTYPE, CELLTYPE,),
                       'awk \'{OFS=\"\\t\"}{print $1, $2, $3, \"PRS\",  0, \"+\", $2, $3, \"%s\"}\' < tmp/%s.PRS.bed  >> tmp/%s.SEGMENTS.bed' % (lightblue,   CELLTYPE, CELLTYPE,),
                       'grep -v \'_\' tmp/%s.SEGMENTS.bed | grep -v \'180915068\' | sortBed -i - | intersectBed -a - -b hg19.bed >> tmp/tmp.bed; rm -f tmp/%s.SEGMENTS.bed; rm -f tmp/%s.*.bed; mv tmp/tmp.bed tmp/%s.SEGMENTATION.bed' % (CELLTYPE, CELLTYPE, CELLTYPE, CELLTYPE,)]
                      #'grep -v \'_\' tmp/%s.SEGMENTS.bed | grep -v \'180915068\' | sortBed -i - | intersectBed -a - -b hg19.bed >> tmp/tmp.bed; rm -f tmp/%s.SEGMENTS.bed; mv tmp/tmp.bed tmp/%s.SEGMENTATION.bed' % (CELLTYPE, CELLTYPE, CELLTYPE,)]
        for command in awkscripts:
            # print command
            subprocess.call(command, shell=True)
        print "SEGMENTATION COMPLETE."
        

    else:
        raise NameError("No available marks for %s." % CELLTYPE)    

